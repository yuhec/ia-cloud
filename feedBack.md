# Feedback

Dans ces derniers cours, j'ai compris l'intérêt du cloud pour faire de l'IA. 
Grâce aux différents outils, aux challenges et au travail en équipe, j'ai pu me faire une opinion contrastée autour de ces technologies.
<br><br>

## 1. Les outils


Grace aux outils "Cloud", on peut obtenir davantage de capacité de stockage et de puissance de calcul. On n'est plus limité par les capacités physiques et hardwares de notre ordinateur. Néanmoins, un outil gratuit n'est pas nécessairement plus performant. On se retrouve assez vite limité sans carte bleue...
<br> <br>
Dans un premier temps, les outils nous ont permis de collaborer plus facilement pour le travail en binôme. On retiendra **Deepnote** qui est facile à prendre en main et où il est possible de voir en temps réel les modifications des collaborateurs. Même si l'outil n'utilise que du CPU dans sa version gratuite, il reste intéressant pour débuter un projet ou pour des projets qui ne demandent pas une énorme puissance de calcul.
<br><br>
Dans un second temps, on s'est tournés vers **Gradient** pour tenter d'obtenir du GPU gratuitement. Je dirai que l'outil permet de dépanner. Sans compte premium, on ne peut créer un container docker -tensorflow 2.0 - GPU FREE - pour seulement 6 heures.

<br>
Enfin, nous avons testé, très rapidement, l'outil **Google Colab**, pendant le démarrage des containers de Gradient. Il semblerait que Google permette l'utilisation gratuite d'un GPU. En revanche, comme Gradient, Google Colab ne permet pas de faire du véritable Pair Programming tant les délais de synchronisation sont longs.

<br><br>
### Résumé
<br>

|         Outils           | + | -
| ------------- |:-------------:| -----:|-----:|
| **Deepnote**    | Synchro en temps réel <br> Services GIT <br> Amazon S3 et Shared Dataset| Un simple CPU <br> 5Go de stockage
| Gradient    |  GPU Free    | Délais de synchro <br> 6h maximum de GPU 
| Google Colab |   GPU Free ?   |  Délais de synchro 

<br><br>

## 2. Les challenges

Durant ces 4 dernières séances de cours, on a pu s'entrainer sur 2 jeux de données : StumbleUpon et Invasive Species. Ces deux challenges sont orientés "Classification". Néanmoins StumbleUpon met l'accent sur des données de textes (url et boilerplates), alors que Invasice Species concerne des images.
<br><br>
Avec le challenge Invasive Species, nous avons appris à traiter les images pour les rendre "lisibles" par un algorithme. On retiendra l'utiliser du framework Tensorflow et des modèles Autokeras (ImageClassifier), plus performant que le modèle sklearn MLPClassifier.
<br><br>

## 3. Travail en binôme

Ici, nous avons exploiter les outils Cloud pour collaborer au mieux pendant cette période pandémique. Nous étions constamment en vocal sur Teams pour échanger sur nos idées et avancer sur le projet, ensemble. Je trouve qu'il est délicat de se donner des tâches différentes et avancer en simultané dans ce type de projet. En effet, les étapes se suivent et il est difficile de les séparer.

De plus, la différence de niveau (voulu dans notre cas) peut parfois dérouter. Je pense sincèrement que Verner et moi avons tout autant appris l'un que l'autre. Grâce aux échos des autres binômes, j'ai le sentiment que ce défi, qu'a été le travail d'équipe avec différents niveaux, nous l'avons relevé avec succès.
<br><br>

## 4 Compétences techniques acquises :
<br>

- Automatiser l'import des données depuis Kaggle
- Automatiser l'export du fichier submission à Kaggle
- Utilisation du fichier requirements.txt
- Découpage d'un projet d'IA (data/ model_builders/ src)
- Créer et importer une classe
- Créer et importer une fonction py dans un autre fichier
- Créer et utiliser une Pipeline
- Sauvegarder un modèle (pickle pour sklearn et autokeras)

<br>
Laëtitia Constantin